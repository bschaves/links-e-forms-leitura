import 'package:flutter/material.dart';
import 'package:links_de_acesso/widgets_list.dart';




class MeuApp extends StatelessWidget {

  String title;

  MeuApp(this.title);

  @override
  Widget build(BuildContext context){
    return MaterialApp(
            title: this.title,
            color: Colors.yellow,
            theme: ThemeData(primarySwatch: Colors.green),
            home: StackFlutter(this.title),
    );
  }
}