// ignore_for_file: use_key_in_widget_constructors
import 'package:flutter/material.dart';
import 'package:links_de_acesso/start_app.dart';

String __version__ = '2024-04-28';

void main() { 
  start_app();
}


void start_app() {
  StatelessWidget meu_app = MeuApp('LINKS DE ACESSO V1.2');
  runApp(meu_app);
}
