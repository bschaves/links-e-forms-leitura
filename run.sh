#!/usr/bin/env bash
#
#===================================================================#
# BUILD PARA LINUX
#===================================================================#
# sudo apt-get install libgtk-3-0 libblkid1 liblzma5
# flutter build linux --release
# ldd build/linux/x64/release/bundle/linux_desktop_test
#
#
#
#
#
# flutter pub add file_picker
# flutter config --android-sdk <path/to/sdk>
# /mnt/ssd-dados/ANDROID-SDK/Sdk

export ANDROID_HOME='/mnt/ssd-dados/ANDROID-SDK'
export ANDROID_SDK_ROOT="${ANDROID_HOME}/Sdk"

function _build_apk()
{
	flutter config --android-sdk "${ANDROID_SDK_ROOT}"
	flutter build apk
}

function _build_linux(){
	flutter build linux --release
	ldd build/linux/x64/release/bundle/links_e_forms_leitura_linux
}

function build_app(){

	#_build_apk
	_build_linux
}

function main(){
	build_app
	#flutter run lib/main.dart -d Linux

}

main "$@"

