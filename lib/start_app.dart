import 'package:flutter/material.dart';
import 'package:links_e_forms_leitura/widgets_app.dart';

final _app_version = '2024-05-18';

void start_app(){
  runApp(MainWidgetApp('Links e Forms - Autor: Bruno Chaves'));
}