import 'package:flutter/material.dart';

void main() {
  runApp(LinksFormsApp('Links e Forms Leitura'));
}


class LinksFormsApp extends StatelessWidget {

  String title_app;
  double default_height = 100.0;
  double default_width = 100.0;

  LinksFormsApp(this.title_app);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.orange,
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.all(20.0),
            margin: EdgeInsets.only(left: 30),
            height: this.default_height,
            width: this.default_width,
            color: Colors.white,
            child: Text('Primeiro Widget'),
          ),
        ),
      ),
    );
  }

}