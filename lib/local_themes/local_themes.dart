import 'package:flutter/material.dart';
import 'dart:io';

class GetLocalIcons {

  final _motoIcon = 'icons/moto.jpg';
  final _ddsIcon = 'icons/dds.jpg';

    GetLocalIcons();

  String getMotoIcon(){
    return this._motoIcon;
  }

  String getDdsIcon(){
    return this._ddsIcon;
  }

}


class DefatultThemes {
  // https://api.flutter.dev/flutter/painting/TextStyle-class.html
  //
  //

  double d_heigth = 80.0;
  double d_width = 250.0;
  double d_width_icons = 80;
  double d_heigth_icons = 55;

  Color defaultBackgroundColor = Colors.white12;
  MaterialColor defaultContainerColor = Colors.deepOrange;
  ThemeData appGlobalTheme = ThemeData.dark(useMaterial3: true);
  TextStyle defataultTextStyle = TextStyle(color: Colors.black.withOpacity(0.9), fontSize: 18.0);
  ButtonStyle defaultBtnStyle = ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll<Color>(Colors.green),
                                      alignment: Alignment.center,

                                  );


  ButtonStyle getBtnStl(){
    return ButtonStyle(
        backgroundColor: MaterialStatePropertyAll<Color>(Colors.green),
        alignment: Alignment.center,
        fixedSize: MaterialStateProperty.all(Size(this.d_width, this.d_heigth)),
        padding: MaterialStatePropertyAll(EdgeInsets.fromLTRB(44, 16, 24, 16)),

    );
  }

  ButtonStyle getBtnStlTwo(){
    return ButtonStyle(
      backgroundColor: MaterialStatePropertyAll<Color>(Colors.orangeAccent),
      alignment: Alignment.center,
      fixedSize: MaterialStateProperty.all(Size(this.d_width, this.d_heigth)),
      padding: MaterialStatePropertyAll(EdgeInsets.fromLTRB(44, 16, 24, 16)),

    );
  }

  TextStyle getTextStl(){
    return TextStyle(
              color: Colors.black.withOpacity(0.8),
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
    );
  }

  TextStyle getTextStlTwo(){
    return TextStyle(
      color: Colors.white,
      backgroundColor: Colors.blue,
      fontSize: 18.0,
    );
  }

}