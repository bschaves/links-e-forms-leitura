import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:links_e_forms_leitura/local_urls.dart';
import 'package:links_e_forms_leitura/local_themes/local_themes.dart';
import 'dart:io';

double default_height = 65.0;
double default_width = 300.0;

GetLocalIcons local_icons = GetLocalIcons();
DefatultThemes local_theme = DefatultThemes();

DefatultThemes getLocalThems(){
  DefatultThemes l_themes = DefatultThemes();
  return l_themes;
}

class MainWidgetApp extends StatelessWidget {

  String title_app;

  MainWidgetApp(this.title_app);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: local_theme.appGlobalTheme,
      home: Scaffold(
        backgroundColor: local_theme.defaultBackgroundColor,
        appBar:
            AppBar(
              centerTitle: true,
              title: Text(
                  this.title_app,
                  selectionColor: Colors.green,
              ),
              backgroundColor: Colors.blueAccent,
              foregroundColor: Colors.white,
            ),
        body: AppBodyButtons().body(),
        )
      );

  }

}



class AppBodyButtons {
  //https://stackoverflow.com/questions/74897463/how-to-add-column-widget-to-row-widget-in-flutter
  // https://docs.flutter.dev/ui/layout

  double d_height = getLocalThems().d_heigth;
  double d_width = getLocalThems().d_width;
  double d_width_icons = getLocalThems().d_width_icons;
  double d_height_icons = getLocalThems().d_heigth_icons;


  Widget body(){
    return SafeArea(
        child:this.getMainRow(),
    );
  }
  
  Widget getMainRow(){
    // https://docs.flutter.dev/cookbook/lists/horizontal-list

    return ListView(
      scrollDirection: Axis.horizontal,
      children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                this.getMainColumn(),
                this.getIconDds()
              ],
              ),
            ),
          ],
    );
  }

  Widget getMainColumn(){

    return SingleChildScrollView(
          child:
          Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            this.getBtnCheckList(), // 1
            this.getVoidBox(),
            this.getBtnVariacao(), // 2
            this.getVoidBox(),
            this.getBtnEntregaFaturas(), // 3
            this.getVoidBox(),
            this.getBtnNegociacao(), // 4
            this.getVoidBox(),
            this.getBtnMedidorDivergente(), // 5
            this.getVoidBox(),
            this.getBtnCobranca(), // 6
            this.getVoidBox(),
            this.getBtnRoteirizacao(), // 7
            this.getVoidBox(),
            this.getBtnAvdRh(), // 8
            this.getVoidBox(),
            this.getBtnMeuRh(), // 9
            this.getVoidBox(),
            this.getBtnTeams(), // 10
            this.getVoidBox(),
            this.getBtnMaps(), // 11
            this.getVoidBox(),
            this.getBtnWhatsApp(), // 12
          ],
    ));

  }

  Widget getIconDds(){
    return SizedBox(
      height: double.infinity,
      //width: 400.0,
      child: Image.asset(
          'icons/dds.jpg',
          fit: BoxFit.cover,
      ),
    );
  }

  Widget getBtnCheckList(){

    return Row(
            children: [
              Image.asset(
                'icons/moto.jpg',
                height: this.d_height_icons, width: this.d_width_icons,
              ),

              TextButton(
                autofocus: true,
                onPressed: openCheckList,
                style:local_theme.getBtnStl(),
                  child: Text(
                    'Forms Check List',
                    selectionColor: Colors.green,
                    style: local_theme.getTextStl(),
                    ),
              ),

            ],

        );
  }

  Widget getVoidBox(){
      return SizedBox(
        width: this.d_width,
        height: 5.0,
      );
  }


  Widget getBtnMeuRh(){
    return Row(
      children: [
        Image.asset(
          'icons/totvs.png',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openMeuRh,
          style:local_theme.getBtnStl(),
          child: Text(
            'Meu RH',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }


  Widget getBtnAvdRh(){
    return Row(
      children: [
        Image.asset(
          'icons/totvs.png',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openAvdRh,
          style:local_theme.getBtnStl(),
          child: Text(
            'Avaliação de desempenho',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnTeams(){
    return Row(
      children: [
        Image.asset(
          'icons/teams.png',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openTeams,
          style:local_theme.getBtnStl(),
          child: Text(
            'Teams Web',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnVariacao(){
    return Row(
      children: [
        Image.asset(
          'icons/variacao1.png',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openVaricao,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Variação de leitura',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnNegociacao(){
    return Row(
      children: [
        Image.asset(
          'icons/negociacao.jpeg',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openNegociacao,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Negociação',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );

  }

  Widget getBtnEntregaFaturas(){
    return Row(
      children: [
        Image.asset(
          'icons/fatura1.jpeg',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openEntregaDeFaturas,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Entrega de faturas',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnRoteirizacao(){
    return Row(
      children: [
        Image.asset(
          'icons/moto.jpg',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openRoteirizacao,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Roteirização',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnMedidorDivergente(){
    return Row(
      children: [
        Image.asset(
          'icons/medidor1.jpeg',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openMedidorDivergente,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Medidor divergente',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnCobranca(){
    return Row(
      children: [
        Image.asset(
          'icons/negociacao.jpeg',
          height: this.d_height, width: 90.0,
        ),
        TextButton(
          autofocus: true,
          onPressed: openCobranca,
          style:local_theme.getBtnStl(),
          child: Text(
            'Forms Cobrança In Loco',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnWhatsApp(){
    return Row(
      children: [
        Image.asset(
          'icons/wp.png',
          height: this.d_height_icons, width: this.d_width_icons,
        ),
        TextButton(
          autofocus: true,
          onPressed: openWhatsApp,
          style:local_theme.getBtnStl(),
          child: Text(
            'WhatsApp',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getBtnMaps(){
    return Row(
      children: [
        Image.asset(
          'icons/m.png',
          height: this.d_height_icons, width: this.d_width_icons,
        ),
        TextButton(
          autofocus: true,
          onPressed: openMaps,
          style:local_theme.getBtnStl(),
          child: Text(
            'Google Maps',
            selectionColor: Colors.green,
            style: local_theme.getTextStl(),
          ),
        ),

      ],

    );
  }

  Widget getTextAuthor(){
    return Center(

      child: Text(
                'Autor: Bruno da Silva Chaves',
                textAlign: TextAlign.center,
                style: local_theme.getTextStlTwo(),
      ),
    );
  }

}

